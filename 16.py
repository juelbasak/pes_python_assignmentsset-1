N = int(input('Enter the value of N : '))

c = 0

for i in range(1, N+1):
    if N % i == 0:
        c += 1
        
if c == 2:
    print('Prime Number')
else:
    print('Not Prime Number')
    
print('Generated Prime Number(s) : ', end = '')    
for i in range(1, N+1):
    c = 0
    for j in range(1, i+1):
        if i % j == 0:
            c += 1
    
    if c == 2:
        print(i, end = ', ')
        
        