a = int(input('Enter a number : '))
b = int(input('Enter another number : '))

print('Bitwise AND : ', a & b)
print('Bitwise OR : ', a | b)
print('Bitwise Not : ', ~a, ~b)
print('Bitwise XOR : ', a ^ b)
print('Bitwise Right Shift : ', a >> b)
print('Bitwise Left Shift : ', a << b)