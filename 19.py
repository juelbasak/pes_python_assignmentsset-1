print('Using For Loop\nEven Numbers : ', end = '')
for i in range(1, 101):
    if i == 50:
        break 
    
    if i == 10 or i == 20 or i == 30 or i == 40 or i == 50:
        continue

    if i % 2 == 0:
        print(i, end = ', ')
    else:
        pass
    
print('\n\nUsing While Loop\nEven Numbers : ', end = '')
i = 1        
while i <= 90:
    if i == 60 or i == 70 or i == 80 or i ==90:
        i += 1
        continue
    
    if i % 2 == 0:
        print(i, end = ', ')
    else:
        pass
        
    i += 1
    
    