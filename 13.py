num1, num2, num3, num4 = map(int, input('Enter 4 numbers : ').split(' '))

l = num1

if (num1 > num2) and (num1 > num3) and (num1 > num4):
    l = num1

elif (num2 > num3) and (num2 > num4):
    l = num2
    
elif (num3 > num4):
    l = num3
    
else:
    l = num4
    
print('Largest : ', l)

num5 = int(input('Enter the 5th number : '))

if num5 > l:
    l = num5
    
print('New largest : ', l)