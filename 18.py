print('------Using For Loop------')

for i in range(1, 101):
    print(i, end = '\n\t\t')
    print(101 - i)
    

print('------Using While Loop------')

i = 1
while i <= 100:
    print(i, end = '\n\t\t')
    print(101 - i)
    
    i += 1
    

mystring ="Hello world"

for i in mystring:
    print(i)
    
    