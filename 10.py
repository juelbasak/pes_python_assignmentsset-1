import random

num1 = random.randint(1, 10)
num2 = random.randint(1, 5)

print(f'Number 1 : {num1}', end = '\t')
print(f'Number 2 : {num2}', end = '\t')

num1 += num2
print(f'Addition : ', num1)

num1 = random.randint(1, 100)
num2 = random.randint(1, 5)

print(f'Number 1 : {num1}', end = '\t')
print(f'Number 2 : {num2}', end = '\t')

num1 -= num2
print('Subtraction : ', num1)

num1 = random.randint(1, 100)
num2 = random.randint(1, 5)

print(f'Number 1 : {num1}', end = '\t')
print(f'Number 2 : {num2}', end = '\t')

num1 *= num2
print('Multiplication : ', num1)

num1 = random.randint(1, 100)
num2 = random.randint(1, 5)

print(f'Number 1 : {num1}', end = '\t')
print(f'Number 2 : {num2}', end = '\t')

num1 /= num2
print('Division : ', num1)

num1 = random.randint(1, 100)
num2 = random.randint(1, 5)

print(f'Number 1 : {num1}', end = '\t')
print(f'Number 2 : {num2}', end = '\t')

num1 %= num2
print('Modulus : ', num1)

num1 = random.randint(1, 100)
num2 = random.randint(1, 5)

print(f'Number 1 : {num1}', end = '\t')
print(f'Number 2 : {num2}', end = '\t')

num1 **= num2
print('Exponent : ', num1)

num1 = random.randint(1, 100)
num2 = random.randint(1, 5)

print(f'Number 1 : {num1}', end = '\t')
print(f'Number 2 : {num2}', end = '\t')

num1 //= num2
print('Floor Division : ', num1)
