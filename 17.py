import random

def maximum(n):
    l = n[0]
    
    for i in n:
        if i > l:
            l = i
    
    return l

def minimum(n):
    s = n[0]

    for i in n:
        if i < s:
            s = i

    return s
    

N = int(input('Enter the value of N : '))
numbers = [random.randint(1, 10) for i in range(N)]
print('The numbers are : ', numbers)


print('Maximum Value : ', maximum(numbers))
print('Minimum Value : ', minimum(numbers))