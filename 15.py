print('----------With Membership Operator----------')

names = ['Jon', 'Bill', 'Maria', 'Jenny', 'Jack']

to_find = 'Maria'

if to_find in names:
    print('Present!')
else:
    print('Not present!')

print('----------Without Membership Operator----------')
flag = 0
for i in names:
    if i == to_find:
        flag = 1
        break

if flag == 1:
    print('Present!')
else:
    print('Not present!')
    
print('Original Names List : ', names)
print('Names list in reverse : ', names[::-1])    

