mylist = list(map(int, input('Enter ten numbers : ').split(' ')))

avg = sum(mylist) / len(mylist)

print('Average : ', avg)

less_than_avg = []
more_than_avg = []
equal_to_avg = []

for i in mylist:
    if i < avg:
        less_than_avg.append(i)
    elif i > avg:
        more_than_avg.append(i)
    else:
        equal_to_avg.append(i)
        
print('Numbers that are less than average : ', less_than_avg)
print('Length : ', len(less_than_avg))

print('Numbers more than average : ', more_than_avg)
print('Length : ', len(more_than_avg))

print('Numbers equal to average : ', equal_to_avg)
print('Length : ', len(equal_to_avg))


